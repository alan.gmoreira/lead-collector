package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepositary produtoRepository;

    public Produto criar(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Produto atualizarPorId(int id, Produto produto) {

        Produto produtoBD = consultarPorId(id);
        produto.setId(id);

        return produtoRepository.save(produto);
    }

    public void deletarPorId(int id) {
        Produto produto = consultarPorId(id);

        produtoRepository.delete(produto);
    }

    public Produto consultarPorId(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent())
            return optionalProduto.get();
        else throw new RuntimeException("Produto não encontrado!");
    }

    public Iterable<Produto> consultarTodos() {
        return produtoRepository.findAll();
    }

}
