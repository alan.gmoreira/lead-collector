package br.com.lead.collector.services;

import br.com.lead.collector.DTO.LeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepositary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private ProdutoRepositary produtoRepositary;


    public Lead save(Lead lead) {
        if(!verificaSeJaExisteLead(lead.getId())) {
            lead.setDataCadastro(LocalDateTime.now());
            lead.setDataUltimaAtualizacao(LocalDateTime.now());
            Lead retornoLead = leadRepository.save(lead);

            return retornoLead;
            //return new LeadDTO(retornoLead.getId(), retornoLead.getNome(), retornoLead.getCpf(), retornoLead.getEmail(), retornoLead.getTelefone());
        }
        else{
            throw new RuntimeException("Ja existe lead cadastrado com esse Id");
        }
    }

    public Iterable<Lead> consultaTotosLeads() {
        return leadRepository.findAll();
    }

    public Lead consultaPorId(int id)
    {
        Optional<Lead> optionalLead =  leadRepository.findById(id);

        if(optionalLead.isPresent())
            return optionalLead.get();
        else
            throw new RuntimeException("O lead não foi encontrado.");
    }

    public void removerLead(int id)
    {
        Lead lead = consultaPorId(id);

        leadRepository.delete(lead);
    }

    public Lead atualizarLead(int id, Lead lead)
    {
        Lead leadDB = consultaPorId(id);

        lead.setDataUltimaAtualizacao(LocalDateTime.now());
        lead.setId(id);

        return leadRepository.save(lead);
    }

    public Lead insereProdutos(int idLead, Iterable<Integer> idsProdutos)
    {
        Lead lead = consultaPorId(idLead);

        List<Produto> produtos = (List<Produto>)produtoRepositary.findAllById(idsProdutos);

        lead.setProdutos(produtos);
        lead.setDataUltimaAtualizacao(LocalDateTime.now());

        return leadRepository.save(lead);
    }

    private boolean verificaSeJaExisteLead(int id)
    {
        return leadRepository.findById(id).isPresent();
    }
}
