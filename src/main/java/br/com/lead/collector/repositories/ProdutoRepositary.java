package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepositary extends CrudRepository<Produto, Integer> {

}
