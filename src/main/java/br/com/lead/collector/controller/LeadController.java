package br.com.lead.collector.controller;

import br.com.lead.collector.DTO.LeadDTO;
import br.com.lead.collector.DTO.ProdutoIdsDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead save(@RequestBody @Valid Lead lead) {
        return leadService.save(lead);
    }

    @GetMapping
    public Iterable<Lead> consultaLeads() {
        return leadService.consultaTotosLeads();
    }

    @GetMapping("/{id}")
    public Lead consultaLeadPorId(@PathVariable(name = "id") int id) {
        try {
            return leadService.consultaPorId(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizaLead(@PathVariable(name = "id") int id, @RequestBody @Valid Lead lead)
    {
        return leadService.atualizarLead(id, lead);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deletarPorId(@PathVariable(name="id") int id)
    {
        leadService.removerLead(id);
    }

    @PutMapping("/adicionarProdutos/{idLead}")
    public Lead adicionarPdorutos(@PathVariable int idLead, @RequestBody ProdutoIdsDTO idsProdutos)
    {
        return leadService.insereProdutos(idLead, idsProdutos.getIdsProdutos());
    }
}
