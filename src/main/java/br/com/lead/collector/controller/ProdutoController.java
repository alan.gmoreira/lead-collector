package br.com.lead.collector.controller;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto criar(@RequestBody @Valid Produto produto)
    {
        return produtoService.criar(produto);
    }

    @PutMapping("/{id}")
    public Produto atualizar(@PathVariable int id, @RequestBody @Valid Produto produto)
    {
        return  produtoService.atualizarPorId(id, produto);
    }

    @GetMapping("/{id}")
    public Produto consultaPorId(@PathVariable int id){
        return  produtoService.consultarPorId(id);
    }

    @GetMapping
    public Iterable<Produto> consultarTodos(){
        return  produtoService.consultarTodos();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarPorId(@PathVariable int id){
        produtoService.deletarPorId(id);
    }


}
