package br.com.lead.collector.DTO;

import java.util.List;

public class ProdutoIdsDTO {
    private List<Integer> idsProdutos;

    public ProdutoIdsDTO() {
    }

    public List<Integer> getIdsProdutos() {
        return idsProdutos;
    }

    public void setIdsProdutos(List<Integer> idsProdutos) {
        this.idsProdutos = idsProdutos;
    }
}
